package net.example;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView text = (TextView) this.findViewById(R.id.textField);
        String res = MyClass.myMethod("ifconfig.me", getCacheDir().getAbsolutePath());
        text.setText(res);
    }
}