This is an example project showcasing how to build Arti to run it on Android and iOS.
For more explanations on what it is doing. You should read Arti documentation [for Android](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/doc/Android.md) and [for iOS](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/doc/iOS.md).

To build for Android:
- install Rust and Android Studio. Make sure you can run an Hello World with both.
- go in the common folder and run `make android`.
- take a coffee, or two.
- open the android folder in Android Studio or use gradle to build your app as usual.

To build for iOS:
- grab a Mac (you can't create an iOS app on a PC)
- install Rust and XCode. Make sure you can run a Hello World with both.
- go in the common folder and run `make ios`.
- take a coffee, or two.
- open the ios folder in XCode and compile your app as usual.
